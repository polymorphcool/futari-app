extends "res://scripts/ui_panel.gd"

var corner = null
var g_editor = null
var cpicker_on = false

func _ready():
	
	_init_pos( false )
	_closed_pos = _open_pos + Vector2(300,0)
	_init_boundingbox()
	
	corner = get_node("corner")
	g_editor = get_node("g_editor")
	
	g_editor.connect( "SIGNAL_OPEN_PICKER", self, "picker_on" )
	g_editor.connect( "SIGNAL_CLOSE_PICKER", self, "picker_off" )

func picker_on():
	cpicker_on = true
	reload_boundingbox()

func picker_off():
	cpicker_on = false
	reload_boundingbox()

func reload_boundingbox():
	_previous_vp = _viewport_size()
	var p = Vector3( 8 + len(g_editor.g_uis) * 24, _previous_vp.y, 50 )
	if cpicker_on:
		p.x -= g_editor.cpicker_offsetx 
	_boundingbox = AABB( p * -1, p + Vector3(0,0,50) )

func _vp_resize():
	var s = _viewport_size()
	var delta_s = s - _previous_vp
	if _init_pos.y != 0:
		_init_pos.y += delta_s.y
		_open_pos.y += delta_s.y
		_closed_pos.y += delta_s.y
		_target_pos.y += delta_s.y
	if _init_pos.x != 0:
		_init_pos.x += delta_s.x
		_open_pos.x += delta_s.x
		_closed_pos.x += delta_s.x
		_target_pos.x += delta_s.x
	_previous_vp = s
	
	g_editor.position.y = -s.y
	g_editor.vp_resize()

func _process(delta):
	
	_process_pos(delta)
	
	if ( !_mouse_in() and _time_open > _max_time_open and cpicker_on ) or Input.is_mouse_button_pressed( BUTTON_RIGHT ):
		g_editor.deselect_previous()
		picker_off()
	
	if self.position.x - _open_pos.x > 200:
		corner.position.x = -( self.position.x - _open_pos.x )
		corner.position.y = 0
	else:
		corner.position.x = self.position.x - 20 * (1 + (self.position.x * 0.005))
		corner.position.y = -20 * (1 + (self.position.x * 0.005))