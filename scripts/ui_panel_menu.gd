extends "res://scripts/ui_panel.gd"

var title = null
var title_on_color = Color( 1,1,1,0.9 )
var title_off_color = Color( 0.15,0.15,0.15,0.5 )
var title_target_color = null
var title_current_color = null

var back = null
var back_hidden_pos = null
var back_visible_pos = null
var back_target_pos = null
var back_current_pos = null

var corner = null

func _ready():
	
	_retracktable = false
	_init_pos( true )
	_init_boundingbox()
	
	title = get_node("title")
	title_target_color = title_on_color
	title_current_color = title_on_color
	back = get_node("main/back")
	back_visible_pos = back.rect_position
	back_hidden_pos = back_visible_pos - Vector2(300,0)
	back_target_pos = back_hidden_pos
	back_current_pos = back_hidden_pos
	back.rect_position = back_current_pos
	
	corner = get_node("corner")

func set_title_color( c ):
	if c != title_on_color:
		title_target_color.r = c.r + title_off_color.r
		title_target_color.g = c.g + title_off_color.g
		title_target_color.b = c.b + title_off_color.b
		title_target_color.a = title_off_color.a
		back_target_pos = back_visible_pos
		_retracktable = true
	else:
		title_target_color = title_on_color
		back_target_pos = back_hidden_pos
		_retracktable = false

func _process(delta):
	
	_process_pos(delta)
	
	if self.position.x < -200:
		corner.position.x = -self.position.x
		corner.position.y = 0
	else:
		corner.position.x = -self.position.x -20 * (1 + (self.position.x * 0.005))
		corner.position.y = -20 * (1 + (self.position.x * 0.005))
	
	title_current_color += ( title_target_color - title_current_color ) * 10 * delta
	title.set( "custom_colors/font_color", title_current_color )
	
	back_current_pos += ( back_target_pos - back_current_pos ) * 10 * delta
	back.rect_position = back_current_pos