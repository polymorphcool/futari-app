extends MeshInstance

func _ready():
	
	if mesh == null:
		return
	
	var p = get_parent()
	var r = p.get_range()
	scale = Vector3( r, r, r )
	mesh.custom_aabb.position = Vector3( -r, -r, -r )
	mesh.custom_aabb.size = Vector3( r*2, r*2, r*2 )
	mesh.custom_aabb.end = Vector3( r, r, r )
	translation = Vector3(0,0,0)