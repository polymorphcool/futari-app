extends "res://scripts/ui_panel.gd"

signal SIGNAL_HANDLE_SELECT
signal SIGNAL_HANDLE_DESELECT

var gradient = null

var bg = null
var tex = null
var handle = null
var handle_c = null
var reset = null

var cpicker_wrapper = null
var cpicker = null

var prev_left_button = false
var initialised = false
var enabled = false

var thickness = 10
var margins = [8,8,23,8]
var prev_vp = null

var handles = []

var sel_handle_enabled = false
var sel_handle = null

var reset_size = null
var bg_init_y = null
var bg_target_y = null
var bg_current_y = null

func clean_up():
	
	set_visible( false )
	
	if ( gradient == null ):
		enabled = false
	
	var oldh = handle_c.get_child(0)
	while oldh:
		handle_c.remove_child( oldh )
		oldh = handle_c.get_child(0)
	handles.clear()

	bg.set_visible( false )
	tex.set_visible( false )
	reset.set_visible( false )

func auto_resize():
	
	# loading current size of viewport for resizing
	prev_vp = _viewport_size()
	
	tex.scale.y = thickness
	tex.position.x = margins[0] + thickness * 0.5
	reset.position.x = margins[0] + thickness * 0.5
	
	# resizing stuff
	var s = _viewport_size()
	var h = s.y - ( margins[0] + margins[2] )
	tex.scale.x = h / gradient.width
	h = tex.scale.x * gradient.width 
	tex.position.y = margins[3] + h * 0.5
	reset.position.y = margins[0] + h + 8
	
	for i in range( 0, len(handles) ):
		var hd = handles[i]
		var offset = gradient.gradient.offsets[i]
		var c = gradient.gradient.colors[i]
		hd["obj"].position.x = margins[3]
		hd["obj"].position.y = margins[0] + s.y - (margins[2] + margins[0]) - h * offset
		if hd["obj"].position.y < margins[0] + 1:
			hd["obj"].position.y = margins[0] + 1
		elif hd["obj"].position.y > margins[0] + h - 1:
			hd["obj"].position.y = margins[0] + h - 1
		hd["obj"].set_visible( true )

func load_gradient( g, cp_w, cp ):
	
	# storing values
	gradient = g
	cpicker_wrapper = cp_w
	cpicker = cp
	
	# loading of references in the scene and basic setup
	# of objects
	if !initialised:
		
		bg = get_node("bg")
		
		tex = get_node("tex")
		tex.rotation_degrees = -90
		
		handle = get_node("handle")
		handle.set_visible( false )
		handle_c = get_node("handle_container")
		
		reset = get_node("reset")
		reset_size = reset.texture.get_size()
		reset.modulate = Color( 1,1,1,0.5 )
		
		initialised = true
	
	# reseting background position
	bg.position.y = -390
	bg_init_y = bg.position.y
	bg_target_y = bg.position.y
	bg_current_y = bg.position.y
	
	# cleanup of previous loading
	clean_up()
	
	if gradient == null or cpicker_wrapper == null or cpicker == null:
		return
	
	# loading texture in sprite
	tex.texture = gradient
	
	enabled = true
	
	# creation of handlers
	for i in range( 0, len( gradient.gradient.offsets) ):
		var offset = gradient.gradient.offsets[i]
		var c = gradient.gradient.colors[i]
		var hnd = handle.duplicate()
		hnd.scale.x = 2
		hnd.scale.y = 2
		hnd.color = c
		handle_c.add_child( hnd )
		var hdata = { 
			"obj": hnd,
			"init_scx": hnd.scale.x, 
			"target_scx": hnd.scale.x, 
			"current_scx": hnd.scale.x,
			"init_color": c
			}
		handles.append( hdata )
	
	auto_resize()
	
	set_visible( true )
	tex.set_visible( true )
	reset.set_visible( true )

func reset_colors():
	for i in range(0,len(handles)):
		var hd = handles[i]
		hd["obj"].color = hd["init_color"]
		gradient.gradient.colors[i] = hd["init_color"]
		if i == sel_handle:
			cpicker.set_pick_color( hd["obj"].color )

func close_handle():
	bg_target_y = bg_init_y
	sel_handle = null

func sync_preprocess(delta):
	
	if gradient == null:
		return
	
	var m = get_viewport().get_mouse_position()
	m -= absolute_pos()
	var inside = false
	if m.x >= margins[3] and m.x < margins[3] + thickness and m.y + 1 >= margins[0] and m.y - margins[0] * 0.5 < prev_vp.y - margins[2]:
		inside = true
	
	var lb = Input.is_mouse_button_pressed( BUTTON_LEFT )
	if !lb and lb != prev_left_button:
		sel_handle_enabled = true
	else:
		sel_handle_enabled = false
	prev_left_button = lb
	
	if !inside:
		if m.x >= reset.position.x - reset_size.x * 0.5 and m.x <= reset.position.x + reset_size.x * 0.5 and m.y >= reset.position.y - reset_size.y * 0.5 and m.y <= reset.position.y + reset_size.y * 0.5 :
			reset.modulate = Color( 1,1,1,1 )
			if sel_handle_enabled:
				reset_colors()
		else:
			reset.modulate = Color( 1,1,1,0.5 )
	
	m.x -= margins[3]
	m.y -= margins[0] * 0.5
	for i in range(0, len(handles) ):
		var hd = handles[i]
		var ho = hd["obj"]
		var hop = ho.position
		if inside and m.y >= hop.y - 10 and m.y <= hop.y + 10:
			if sel_handle_enabled and sel_handle != i:
				sel_handle = i
				cpicker.set_pick_color( hd["obj"].color )
				bg_target_y = int( hd["obj"].position.y - 195 )
				if bg_target_y > prev_vp.y - 390 - margins[2]:
					bg_target_y = prev_vp.y - 390 - margins[2]
				elif bg_target_y < margins[0]:
					bg_target_y = margins[0]
				bg.set_visible( true )
				hd["target_scx"] = margins[0]
				emit_signal( "SIGNAL_HANDLE_SELECT", self )
				continue
			elif sel_handle_enabled and sel_handle == i:
				emit_signal( "SIGNAL_HANDLE_DESELECT", self )
				close_handle()
			if sel_handle != i:
				hd["target_scx"] = margins[0] * 0.5
		elif !sel_handle_enabled and sel_handle != i:
			hd["target_scx"] = hd["init_scx"]

func sync_process(delta):
	
	if gradient == null:
		return
	
	for hd in handles:
		hd["current_scx"] += ( hd["target_scx"] - hd["current_scx"] ) * 20 * delta
		hd["obj"].scale.x = hd["current_scx"]
	
	if sel_handle != null:
		var c = cpicker.get_pick_color()
		bg.color = c
		handles[sel_handle]["obj"].color = c
		gradient.gradient.colors[sel_handle] = c
	
	if bg_current_y != bg_target_y:
		bg_current_y += ( bg_target_y - bg_current_y ) * 20 * delta
		if abs( bg_current_y - bg_target_y ) < 0.1:
			bg_current_y = bg_target_y
			if bg_current_y == bg_init_y:
				bg.visible = false
		bg.position.y = bg_current_y
