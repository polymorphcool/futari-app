extends Spatial

var g_editor = null

func _ready():
	
	g_editor = get_node("menu/g_editor")
	vp_resize()
	
	get_viewport().connect( "size_changed", self, "vp_resize" )

func vp_resize():
	g_editor.load_gradient( get_node("height_vortex").height_tex )
	g_editor.load_gradient( get_node("height_vortex").height_tex )