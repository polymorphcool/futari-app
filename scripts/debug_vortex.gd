extends MeshInstance

func _ready():
	
	if mesh == null:
		return
	
	var p = get_parent()
	var r = p.get_range()
	var h = p.get_height()
	scale = Vector3( r,h,r )
	translation = Vector3(0,0,0)