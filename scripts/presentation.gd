extends Spatial

const USER_CONFIG = "user://settings.cfg"

var loaded_path = ""
var loaded_scene = null

var cam = null
var axis = null
var axis_parts = null
var axis_display = true
var menu = null
var info = null
var params = null
var g_editor = null
var cursor_x = null
var cursor_y = null
var cursor_box = null

var bg_default = null
var bg_target = null
var bg_current = null

var presets = null

var prev_mp = null
var start_mp = null
var min_mp = null
var max_mp = null
var max_scale = null
var current_scale = Vector2()
var max_still_mp = 0.5
var still_mp = 0
var open_speed_a = 2
var close_speed_a = 5

var vp = null
var screen_3d = null
var screen_shader = null
var shader_time = 0

var axis_scale_default = 3000
var axis_scale_target = null
var axis_scale_current = null

func _ready():
	# loading globals
	vp = get_viewport().size
	# getting nodes
	screen_3d = get_node("vpc/screen_3d")
	screen_shader = get_node("vpc").material
	cam = get_node("vpc/screen_3d/cam")
	axis = get_node("vpc/screen_3d/axis")
	axis_parts = [ axis.get_node("axis_x"), axis.get_node("axis_y"), axis.get_node("axis_z") ]
	axis_scale_target = Vector3( axis_scale_default, axis_scale_default, axis_scale_default )
	axis_scale_current = axis_scale_target
	menu = get_node("menu")
	info = get_node("info")
	params = get_node("params")
	g_editor = get_node("params/g_editor")
	cursor_x = get_node("mouse_cursor/mx")
	cursor_y = get_node("mouse_cursor/my")
	cursor_box = []
	for i in range(0,4):
		var n = get_node("mouse_cursor/mb" + str(i))
		cursor_box.append( n )
		cursor_box[i].color = Color( 1,1,1,0 )
	cursor_box_init()
	# loading default
	bg_default = cam.get_environment().background_color
	bg_target = bg_default
	bg_current = bg_default
	# loading configuration
	var conf = ConfigFile.new()
	conf.load( USER_CONFIG )
	show_axis( conf.get_value( "world", "axis", true ) )
	if !axis_display:
		axis_scale_target = Vector3(0,0,0)
		axis_scale_current = Vector3(0,0,0)
		axis.set_visible( false )
	get_node("info/main/axis").pressed = conf.get_value( "world", "axis", true )
	fullscreen( conf.get_value( "world", "fullscreen", false ) )
	get_node("info/main/fullscreen").pressed = conf.get_value( "world", "fullscreen", false )
	presets = conf.get_value( "scenes", "user", {} )
	load_scene( conf.get_value( "scenes", "loaded", loaded_path ) )
	# connect buttons
	get_node("menu/main/back").connect( "pressed", self, "load_bck" )
	get_node("menu/main/attractors").connect( "pressed", self, "load_actrs" )
	get_node("menu/main/winds").connect( "pressed", self, "load_ws" )
	get_node("menu/main/wind_gradient").connect( "pressed", self, "load_wg" )
	get_node("menu/main/vortex_simple").connect( "pressed", self, "load_vs" )
	get_node("menu/main/vortex_gradient").connect( "pressed", self, "load_vg" )
	get_node("menu/main/eruption").connect( "pressed", self, "load_erpt" )
	get_node("info/main/reset_camera").connect( "pressed", self, "reset_camera" )
	get_node("info/main/axis").connect( "toggled", self, "show_axis" )
	get_node("info/main/fullscreen").connect( "toggled", self, "fullscreen" )
	## connect resize event
	get_viewport().connect( "size_changed", self, "vp_resize" )
	# disable default behaviour to store presets before quit
	get_tree().set_auto_accept_quit(false)

func cursor_box_init():
	for i in range(0,4):
		if i % 2 == 0:
			cursor_box[i].position.y = ( vp.y - 2 ) * 0.5
			cursor_box[i].scale.y = vp.y - 2
		else:
			cursor_box[i].position.x = vp.x * 0.5
			cursor_box[i].scale.x = vp.x

func _process(delta):
	
	bg_current += ( bg_target - bg_current ) * 4 * delta
	cam.environment.background_color = bg_current
	# rounding mouse position to 40
	var mp = get_viewport().get_mouse_position()
	var menu_hover = menu._mouse_in() or info._mouse_in() or params._mouse_in()
	if Input.is_mouse_button_pressed( BUTTON_RIGHT ):
		if still_mp < max_still_mp:
			still_mp = max_still_mp
		else:
			still_mp += delta
	elif prev_mp == null || prev_mp == mp:
		if !menu_hover:
			still_mp += delta
	else:
		still_mp = 0
		if start_mp == null:
			cursor_x.position = mp
			cursor_y.position = mp
			start_mp = mp
			min_mp = mp
			max_mp = mp
			max_scale = Vector2()
		else:
			var delta_mp = mp - start_mp
			if min_mp.x > mp.x:
				min_mp.x = mp.x
			if min_mp.y > mp.y:
				min_mp.y = mp.y
			if max_mp.x < mp.x:
				max_mp.x = mp.x
			if max_mp.y < mp.y:
				max_mp.y = mp.y
			max_scale = max_mp - min_mp
			current_scale = max_scale
			screen_shader.set_shader_param( "_min", min_mp / vp )
			screen_shader.set_shader_param( "_max", max_mp / vp )
	
	var ratio = 1
	var speed_a = open_speed_a
	if still_mp > max_still_mp:
		ratio = 0
		speed_a = close_speed_a
	cursor_x.color.a += ( ratio - cursor_x.color.a ) * speed_a * delta
	cursor_y.color.a += ( ratio - cursor_y.color.a ) * speed_a * delta
	
	if start_mp != null and !Input.is_mouse_button_pressed( BUTTON_RIGHT ):
		cursor_x.position = Vector2( mp.x, start_mp.y - ( start_mp.y - min_mp.y ) + max_scale.y * 0.5 )
		cursor_y.position = Vector2( start_mp.x - ( start_mp.x - min_mp.x ) + max_scale.x * 0.5, mp.y )
		cursor_box[0].position.x = min_mp.x * 0.5
		cursor_box[0].position.y = min_mp.y * 0.5 + max_mp.y * 0.5
		cursor_box[1].position.y = min_mp.y * 0.5
		cursor_box[2].position.x = max_mp.x + (vp.x-max_mp.x) * 0.5
		cursor_box[2].position.y = min_mp.y * 0.5 + max_mp.y * 0.5
		cursor_box[3].position.y = max_mp.y + (vp.y-max_mp.y) * 0.5
		
	if still_mp > max_still_mp and max_scale != null:
		current_scale = max_scale * cursor_x.color.a
		
	cursor_x.scale.y = current_scale.y
	cursor_y.scale.x = current_scale.x
	if min_mp != null:
		cursor_box[0].scale.x = min_mp.x
		cursor_box[1].scale.y = min_mp.y
		cursor_box[2].scale.x = vp.x-max_mp.x
		cursor_box[3].scale.y = vp.y-max_mp.y
		for i in range(0,4):
			if i % 2 == 0:
				cursor_box[i].scale.y = max_scale.y
			var at = ( cursor_x.color.a * 0.5 - cursor_box[i].color.a ) * close_speed_a * delta
			if menu_hover:
				at = -cursor_box[i].color.a * close_speed_a * delta
			cursor_box[i].color.a += at
	
	if cursor_x.color.a < 0.03 and start_mp != null:
		start_mp = null
	
	prev_mp = mp
	
	shader_time += delta
	screen_shader.set_shader_param( "_time", rand_range(-1,1) )
	screen_shader.set_shader_param( "_strength", cursor_box[0].color.a * 2 )
	screen_shader.set_shader_param( "_bg", bg_current )
	
	if axis_scale_current != axis_scale_target:
		var cl = axis_scale_current.length()
		var tl = axis_scale_target.length()
		if cl > tl:
			axis_scale_current += ( axis_scale_target - axis_scale_current ) * 15 * delta
		else:
			axis_scale_current += Vector3( 1000, 1000, 1000 ) * delta
			if axis_scale_current.length() > tl:
				axis_scale_current = axis_scale_target
		if (axis_scale_current - axis_scale_target).length() < 0.1:
			axis_scale_current = axis_scale_target
		if axis_scale_current.length() == 0:
			axis.set_visible( false )
		else:
			axis_parts[0].scale.x = axis_scale_current.x
			axis_parts[1].scale.y = axis_scale_current.y
			axis_parts[2].scale.z = axis_scale_current.z

func _input(event):
	# QUIT
	if(event.is_action_pressed("ui_quit")):
		store_presets()
		var conf = ConfigFile.new()
		conf.set_value( "world", "axis", axis_display )
		conf.set_value( "world", "fullscreen", OS.is_window_fullscreen() )
		conf.set_value( "scenes", "loaded", loaded_path )
		conf.set_value( "scenes", "user", presets )
		conf.save( USER_CONFIG )
		get_tree().quit()

#func _notification(what):
#    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
#        get_tree().quit() # default behavior

func reset_camera():
	cam.reset_cam()
	cam.targetd = cam_distance()

func show_axis( enabled ):
	axis_display = enabled
	var m = 1
	if !enabled:
		m = 0
	axis_scale_target = Vector3( axis_scale_default * m, axis_scale_default * m, axis_scale_default * m )
	if axis_scale_target.length() > 0:
		axis.set_visible( true )

func fullscreen( enabled ):
	OS.set_window_fullscreen( enabled )
	still_mp = 1
	cursor_x.color.a = 0
	cursor_y.color.a = 0
	for i in range(0,4):
		cursor_box[i].color.a = 0

func vp_resize():
	vp = get_viewport().size
	screen_3d.get_parent().rect_size = vp
	cursor_box_init()
	menu._vp_resize()
	info._vp_resize()
	params._vp_resize()

func cam_distance():
	if loaded_path == "res://scenes/eruption.tscn":
		return 2000
	elif loaded_path == "res://scenes/vortex_gradient.tscn":
		return 1200
	elif loaded_path == "res://scenes/vortex_simple.tscn":
		return 800
	elif loaded_path == "res://scenes/wind_gradient.tscn":
		return 400
	elif loaded_path == "res://scenes/winds.tscn":
		return 850
	elif loaded_path == "":
		return 200
	else:
		return 500

func get_bg_color():
	if loaded_path == "res://scenes/eruption.tscn":
		return Color( 0.65, 0.65, 0.65 )
	elif loaded_path == "res://scenes/vortex_gradient.tscn":
		return Color( 0.5, 0.5, 0.5 )
	elif loaded_path == "res://scenes/vortex_simple.tscn":
		return Color( 0.1, 0.1, 0.1 )
	elif loaded_path == "res://scenes/wind_gradient.tscn":
		return Color( 0.55, 0.55, 0.55 )
	elif loaded_path == "res://scenes/winds.tscn":
		return Color( 0.5, 0.5, 0.5 )
	elif loaded_path == "res://scenes/attractors.tscn":
		return Color( 0.15, 0.15, 0.15 )
	else:
		return bg_default

#func dump_tree( o, lvl ):
#	var s = " "+o.name
#	for i in range( 0, lvl ):
#		s = "--"+s
#	print(s)
#	for i in range( 0, o.get_child_count() ):
#		dump_tree( o.get_child(i), lvl + 1 )
		
func load_gradients():
	g_editor.clean_up()
	var n
	if loaded_path == "res://scenes/eruption.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
		n = get_node( "vpc/screen_3d/root/complex_vortex" )
		g_editor.load_gradient( n.top_tex )
		g_editor.load_gradient( n.center_tex )
		g_editor.load_gradient( n.bottom_tex )
		g_editor.load_gradient( n.height_tex )
	elif loaded_path == "res://scenes/vortex_gradient.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
		n = get_node( "vpc/screen_3d/root/height_vortex" )
		g_editor.load_gradient( n.height_tex )
	elif loaded_path == "res://scenes/vortex_simple.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
	elif loaded_path == "res://scenes/wind_gradient.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
		n = get_node( "vpc/screen_3d/root/FutariWind" )
		g_editor.load_gradient( n.strength_decay_texture )
		n = get_node( "vpc/screen_3d/root/FutariWind2" )
		g_editor.load_gradient( n.strength_decay_texture )
	elif loaded_path == "res://scenes/winds.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
		n = get_node( "vpc/screen_3d/root/FutariParticles2" )
		g_editor.load_gradient( n.process_material.color_ramp )
	elif loaded_path == "res://scenes/attractors.tscn":
		n = get_node( "vpc/screen_3d/root/FutariParticles" )
		g_editor.load_gradient( n.process_material.color_ramp )
	else:
		pass
	params.reload_boundingbox()

func load_bck():
	load_scene( "" )
	menu.set_title_color( menu.title_on_color )
	menu._retracktable = false
	menu.back_target_pos = menu.back_hidden_pos

func load_erpt():
	load_scene( "res://scenes/eruption.tscn" )

func load_vg():
	load_scene( "res://scenes/vortex_gradient.tscn" )
	
func load_vs():
	load_scene( "res://scenes/vortex_simple.tscn" )

func load_wg():
	load_scene( "res://scenes/wind_gradient.tscn" )

func load_ws():
	load_scene( "res://scenes/winds.tscn" )

func load_actrs():
	load_scene( "res://scenes/attractors.tscn" )

func load_scene( path ):
	if loaded_path == path:
		return false
	if loaded_scene != null:
		# storage of current scene presets
		store_presets()
		screen_3d.remove_child( loaded_scene )
	loaded_path = path
	if loaded_path == "":
		loaded_scene = null
	else:
		loaded_scene = load( path ).instance()
		screen_3d.add_child( loaded_scene )
	if not restore_presets():
		cam.targetd = cam_distance()
		bg_target = get_bg_color()
	menu.set_title_color( bg_target )
	load_gradients()
	return true

func store_presets():
	presets[ loaded_path ] = {}
	#presets[ loaded_path ][ "scene" ] = loaded_scene.get_presets()
	presets[ loaded_path ][ "cam" ] = cam.get_presets()
	presets[ loaded_path ][ "bg" ] = bg_target

func restore_presets():
	if presets.has( loaded_path ):
		cam.load_presets( presets[ loaded_path ][ "cam" ] )
		bg_target = presets[ loaded_path ][ "bg" ]
		return true
	else:
		cam.reset_cam()
		return false