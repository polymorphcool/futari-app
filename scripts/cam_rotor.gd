extends Camera

var rotx = -0.3
var roty = 0.6
var defaultp = Vector2( rotx, roty )
var targetp = null
var currentp = null
var mousep = null
var dirp = Vector2( 0,0 )

var defaultd = null
var targetd = null
var currentd = null
var dird = 0

var axx = null
var axy = null
var axz = null
var axis_thickness = null

func _ready():
	
	defaultd = translation.length()
	targetd = defaultd
	currentd = defaultd
	targetp = defaultp
	currentp = defaultp
	
	axx = get_parent().get_node("axis/axis_x")
	axy = get_parent().get_node("axis/axis_y")
	axz = get_parent().get_node("axis/axis_z")
	
	axis_thickness = axx.scale.y / defaultd

func rot_modulo( v ):
	while v.x > PI:
		v.x -= PI
	while v.y > PI:
		v.y -= PI
	while v.x < -PI:
		v.x += PI
	while v.y < -PI:
		v.y += PI
	return v

func reset_cam():
	#currentp = rot_modulo( currentp )
	targetp = defaultp
	targetd = defaultd

func load_presets( presets ):
	if presets.has( "targetp" ):
		targetp = presets["targetp"]
	if presets.has( "targetd" ):
		targetd = presets["targetd"]

func get_presets():
	targetp = rot_modulo( targetp )
	return { "targetp": targetp, "targetd": targetd }

func get_joy_axis( pad, x_axis, y_axis ):
	var deadzone = 0.15
	var axis = Vector2( Input.get_joy_axis( pad, x_axis ), Input.get_joy_axis( pad, y_axis ) )
	if abs( axis.x ) < deadzone:
		axis.x = 0
	elif axis.x < 0:
		axis.x += deadzone
	elif axis.x > 0:
		axis.x -= deadzone
	if abs( axis.y ) < deadzone:
		axis.y = 0
	elif axis.y < 0:
		axis.y += deadzone
	elif axis.y > 0:
		axis.y -= deadzone
	return axis * ( 1 + deadzone )
	
func _process(delta):
	
	var left_axis = get_joy_axis( 0, JOY_AXIS_2, JOY_AXIS_3 )
	if left_axis.length() > 0:
		targetp.x -= 3 * left_axis.y / 180 * PI
		targetp.y -= 3 * left_axis.x / 180 * PI
	
	if Input.is_mouse_button_pressed( BUTTON_LEFT ) :
#		print( "LEFT" )
		pass
	if Input.is_mouse_button_pressed( BUTTON_RIGHT ) :
		var mp = get_viewport().get_mouse_position()
		var deltamp = Vector2(0,0)
		if mousep != null:
			deltamp = mp - mousep
		targetp.x -= deltamp.y / 180 * PI
		targetp.y -= deltamp.x / 180 * PI
		mousep = mp
	else:
		mousep = null
	
	var deltap = targetp - currentp
	if deltap.length() > 1:
		deltap.normalized()
	
	dirp += ( deltap - dirp ) * 5 * delta
	currentp += dirp * delta
	
	var deltad = targetd - currentd
	if deltad > 500:
		deltad = 500
	elif deltad < -500:
		deltad = -500
	dird += ( deltad - dird ) * 50 * delta
	currentd += dird * delta
	
	var m = Basis()
	m = m.rotated( Vector3( 1,0,0), currentp.x )
	m = m.rotated( Vector3( 0,1,0), currentp.y )
	
	var dir = m.xform( Vector3(0,0,1) ) * currentd
	translation = dir
	look_at( Vector3(0,0,0), m.xform( Vector3(0,1,0) ) )
	
	axx.scale.y = axis_thickness * currentd
	axx.scale.z = axis_thickness * currentd
	axy.scale.x = axis_thickness * currentd
	axy.scale.z = axis_thickness * currentd
	axz.scale.x = axis_thickness * currentd
	axz.scale.y = axis_thickness * currentd
