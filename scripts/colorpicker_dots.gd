extends Container

var cpicker = null
var prev_color = null
var dots = []
	
func _ready():
	
	var dot = get_node("dot")
	cpicker = get_node("colorpicker")
	cpicker.rect_position.y -= 17
	var c = cpicker.get_pick_color()
	var gap = 255.0 / 20
	for y in range( 0, 21 ):
		for x in range( 0, 21 ):
			var d = dot.duplicate()
			self.add_child( d )
			d.position.x = 1 + x * gap
			d.position.y = 1 + y * gap
			d.color = c
			dots.append( d )
	prev_color = Vector3( c.r, c.g, c.b )
	remove_child( dot )

func _process(delta):
	
	if !visible:
		return
	
	var c = cpicker.get_pick_color()
	var cv3 = Vector3( 1 - c.r, 1 - c.g, 1 - c.b )
	if cv3 != prev_color:
		prev_color += ( cv3 - prev_color ) * 2 * delta
		c = Color( prev_color.x, prev_color.y, prev_color.z )
		for d in dots:
			d.color = c