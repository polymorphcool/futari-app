extends "res://scripts/ui_panel.gd"

var corner = null

func _ready():
	
	_init_pos( false )
	_init_boundingbox()
	
	corner = get_node("corner")

func _process(delta):
	
	_process_pos(delta)
	
	if self.position.x < -200:
		corner.position.x = -self.position.x
		corner.position.y = 0
	else:
		corner.position.x = -self.position.x -20 * (1 + (self.position.x * 0.005))
		corner.position.y = 20 * (1 + (self.position.x * 0.005))