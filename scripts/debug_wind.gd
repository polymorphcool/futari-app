extends MeshInstance

func _ready():
	
	if mesh == null:
		return
	
	var p = get_parent()
	var s = log( p.get_strength() )
	scale = Vector3( s,s,s )
	rotate_x( -PI * 0.5 )
	translation = Vector3(0,0, -p.get_range() - s)