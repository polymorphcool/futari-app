extends Label

const TIMER_LIMIT = 0.5
var timer = 0.0

func _ready():
	pass

func _process(delta):
	timer += delta
	if timer > TIMER_LIMIT:
		timer = 0.0
		text = str(Engine.get_frames_per_second())