extends "res://scripts/ui_panel.gd"

signal SIGNAL_OPEN_PICKER
signal SIGNAL_CLOSE_PICKER

var map_gradient = {}
var sel_gui = [null,null]
var g_ui_tmpl = null
var g_wrapper = null
var g_uis = []
var cpicker_wrapper = null
var cpicker = null

var cpicker_offsetx = -304

func _ready():
	
	g_ui_tmpl = get_node( "g_ui_tmpl" )
	if !g_ui_tmpl.has_method("load_gradient"):
		g_ui_tmpl = null
		print( "g_ui_tmpl object MUST have the script g_ui.gd attached!" )
		return
	g_wrapper = get_node( "g_wrapper" )
	cpicker_wrapper = get_node( "cp_wrapper" )
	cpicker = get_node( "cp_wrapper/colorpicker" )
	
	get_viewport().connect( "size_changed", self, "vp_resize" )

func vp_resize():
	for m in map_gradient:
		map_gradient[m].auto_resize()

func clean_up():
	
	if g_wrapper.get_child_count() == 0:
		return
	var oldh = g_wrapper.get_child(0)
	while oldh:
		g_wrapper.remove_child( oldh )
		oldh = g_wrapper.get_child(0)
	g_uis.clear()
	map_gradient.clear()
	
	sel_gui[0] = null
	sel_gui[1] = null
	
	cpicker_wrapper.set_visible( false )
	cpicker_wrapper.rect_position.y = -390

func load_gradient( g ):
	
	if g == null or g_ui_tmpl == null or map_gradient.has( g ):
		return
	
	# creation of a g_ui dedicated to gradient
	var g_ui = g_ui_tmpl.duplicate()
	# loading gradient and other references
	g_ui.load_gradient( g, cpicker_wrapper, cpicker )
	g_wrapper.add_child( g_ui )
	
	# position of g_ui
	g_ui.auto_resize()
	
	# storing gradient - g_ui key-value
	map_gradient[ g ] = g_ui
	g_uis.append( g_ui )
	
	# reposition all g_uis
	for i in range( 0, len(g_uis) ):
		g_uis[i].position.x = -( 8 + 24 * ( len(g_uis) - i ) )
	
	# connect signals
	g_ui.connect( "SIGNAL_HANDLE_SELECT", self, "handle_selected_cb" )
	g_ui.connect( "SIGNAL_HANDLE_DESELECT", self, "handle_deselected_cb" )

func find_obj( obj ):
	var gid = -1 
	var i = 0
	for g in g_uis:
		if g == obj:
			gid = i
			break
		i += 1
	return gid

func deselect_previous():
	if sel_gui[0] != null:
		g_uis[sel_gui[0]].close_handle()

func handle_selected_cb( obj ):
	var gid = find_obj( obj )
	if gid == -1:
		return
	var jump_picker = false
	# initialisation of the color picker
	cpicker_wrapper.set_visible( true )
	# reseting previous g_ui
	if sel_gui[0] != null and sel_gui[0] != gid:
		deselect_previous()
		jump_picker = true
	elif sel_gui[0] == null:
		jump_picker = true
	# storage of new values
	sel_gui[0] = gid
	sel_gui[1] = obj.sel_handle
	if jump_picker:
		cpicker_wrapper.rect_position = Vector2( g_uis[sel_gui[0]].position.x + cpicker_offsetx, -390 )
	emit_signal( "SIGNAL_OPEN_PICKER" )

func handle_deselected_cb( obj ):
	var gid = find_obj( obj )
	if sel_gui[0] != gid:
		deselect_previous()
	if gid == -1:
		return
	sel_gui[0] = gid
	sel_gui[1] = null
	emit_signal( "SIGNAL_CLOSE_PICKER" )

func _process(delta):
	
	for g in g_uis:
		g.sync_preprocess( delta )
	
	if sel_gui[0] != null:
		cpicker_wrapper.rect_position.y = g_uis[sel_gui[0]].bg.position.y
	
	for g in g_uis:
		g.sync_process( delta )