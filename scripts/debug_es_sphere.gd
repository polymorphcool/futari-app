extends MeshInstance

func _ready():
	
	var p = get_parent()
	
	if p.process_material == null or mesh == null:
		return
	
#	var size = p.process_material.emission_box_extents
	var r = p.process_material.emission_sphere_radius
	scale = Vector3( r,r,r )
	translation = Vector3(0,0,0)