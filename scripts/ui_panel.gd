extends Node2D

var _init_pos = null
var _open_pos = null
var _closed_pos = null
var _target_pos = null
var _current_pos = null

var _retracktable = true
var _max_time_open = 0.5
var _time_open = 0

var _boundingbox_offset = Vector3(0,0,0)
var _boundingbox = null

var _previous_vp = Vector2( 1024, 600 )

func _init_pos( open ):
	_previous_vp = _viewport_size()
	_init_pos = self.position
	_open_pos = self.position
	_closed_pos = _open_pos - Vector2(300,0)
	if open:
		_target_pos = _open_pos
		_current_pos = _open_pos
	else:
		_target_pos = _closed_pos
		_current_pos = _closed_pos
		self.position = _current_pos

func _init_boundingbox():
	_boundingbox = AABB()
	_append_bounding_box( self )
	_boundingbox = _boundingbox.grow( 50 )

func _append_bounding_box( n ):
	if n is Button or n is Label:
		_boundingbox = _boundingbox.expand( Vector3( n.rect_position.x, n.rect_position.y, 0 ) )
		_boundingbox = _boundingbox.expand( Vector3( n.rect_position.x + n.rect_size.x, n.rect_position.y + n.rect_size.y, 0 ) )
	for i in range(0, n.get_child_count()):
		_append_bounding_box( n.get_child(i) )

func _vp_resize():
	var s = _viewport_size()
	var delta_s = s - _previous_vp
	if _init_pos.y != 0:
		_init_pos.y += delta_s.y
		_open_pos.y += delta_s.y
		_closed_pos.y += delta_s.y
		_target_pos.y += delta_s.y
	if _init_pos.x != 0:
		_init_pos.x += delta_s.x
		_open_pos.x += delta_s.x
		_closed_pos.x += delta_s.x
		_target_pos.x += delta_s.x
	_previous_vp = s

func _process_pos(delta):
	if Input.is_mouse_button_pressed( BUTTON_RIGHT ):
		_target_pos = _closed_pos
	elif _retracktable and !_mouse_in():
		_time_open += delta
		if _time_open > _max_time_open:
			_target_pos = _closed_pos
	else:
		_time_open = 0
		_target_pos = _open_pos
	_current_pos += ( _target_pos - _current_pos ) * 10 * delta
	self.position = _current_pos

func _mouse_in():
	if _boundingbox == null:
		return false
	return _boundingbox.has_point( _mouse_vec3() )

func _mouse_pos():
	return get_viewport().get_mouse_position() - _init_pos

func _mouse_vec3():
	var v2 = _mouse_pos()
	return Vector3( v2.x, v2.y, 0 )
	
func _viewport_size():
	var v = get_viewport()
	if v == null:
		return _previous_vp
	return get_viewport().size

func absolute_pos():
	return get_global_transform_with_canvas().origin