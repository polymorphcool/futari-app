shader_type canvas_item;
render_mode blend_mix;

uniform float _time;
uniform float _strength;
uniform vec2 _min;
uniform vec2 _max;
uniform vec4 _bg;

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void fragment(){
	vec2 iuv = vec2( UV.x, 1.0 - UV.y );
	vec4 original = texture( TEXTURE, iuv );
	if ( _min.x <= iuv.x && _min.y <= iuv.y && _max.x >= iuv.x && _max.y >= iuv.y ) {
		COLOR = original;
	} else {
		float l = dot(vec3(0.30, 0.59, 0.11), original.xyz);
		l = float( int( l * 255.0 ) / 20 ) * 0.078431373;
//		l = ( ( float( int( l * 255.0 ) / 25 ) * 0.074 ) - _bg.x ) / (1.0-_bg.x);
//		float s = _strength * length( _bg.xyz - original.xyz );
//		COLOR = mix( original, vec4(l,l,l,1), min( 1.0 , s ) );
		COLOR = mix( original, mix( _bg, vec4(l,l,l,1), min( 1.0, length( _bg.xyz - original.xyz ) * 2.0 ) ), min( 1.0, _strength ) );
	}
}