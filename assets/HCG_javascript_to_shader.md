# HCG color system

**HCG**, hue/chroma/grey, is a color system derived from [Munsell color system](https://en.wikipedia.org/wiki/Munsell_color_system).

## conversion of d3-hcg javascript implementation [FUNCTIONAL]

### js

original file: [github.com](https://github.com/acterhd/hcg-color/blob/master/convert/index.js)

```javascript

import {color, rgb} from "d3-color";

function hcgConvert(o) {
	if (o instanceof Hcg) return new Hcg(o.h, o.c, o.g, o.opacity);
	if (!(o instanceof rgb)) o = rgb(o);
	var r = o.r / 255,
		g = o.g / 255,
		b = o.b / 255,
		min = Math.min(r, g, b),
		max = Math.max(r, g, b),
		d = max - min,
		h = NaN,
		gr = min && min / (1 - d);	
	if (d) {
		if (r === max) h = (g - b) / d + (g < b) * 6;
		else if (g === max) h = (b - r) / d + 2;
		else h = (r - g) / d + 4;
		h *= 60;
	}
	return new Hcg(h, d, gr, o.opacity);
}

export default function hcg(h, c, g, opacity) {
	return arguments.length === 1 ? hcgConvert(h) : new Hcg(h, c, g, opacity == null ? 1 : opacity);
}

export function Hcg(h, c, g, opacity) {
	this.h = +h;
	this.c = +c;
	this.g = +g;
	this.opacity = +opacity;
}

var hcgPrototype = Hcg.prototype = hcg.prototype = Object.create(color.prototype);

hcgPrototype.constructor = Hcg;

hcgPrototype.rgb = function() {
	var h = isNaN(this.h) ? 0 : this.h % 360 + (this.h < 0) * 360,
		c = this.c,
		g = isNaN(this.g) ? 0 : this.g,
		a = this.opacity,
		x = c * (1 - Math.abs((h / 60) % 2 - 1)),
		m = g * (1 - c);
	return h < 60 ? hcg2rgb(c, x, 0, m, a)
		: h < 120 ? hcg2rgb(x, c, 0, m, a)
		: h < 180 ? hcg2rgb(0, c, x, m, a)
		: h < 240 ? hcg2rgb(0, x, c, m, a)
		: h < 300 ? hcg2rgb(x, 0, c, m, a)
		: hcg2rgb(c, 0, x, m, a);
};

hcgPrototype.displayable = function() {
	return (0 <= this.g && this.g <= 1 || isNaN(this.g))
		&& (0 <= this.c && this.c <= 1)
		&& (0 <= this.opacity && this.opacity <= 1);
};

function hcg2rgb(r1, g1, b1, m, a) {
	return rgb((r1 + m) * 255, (g1 + m) * 255, (b1 + m) * 255, a);
}

```

### opengl es 3 shader

![](https://polymorph.cool/wp-content/uploads/2019/04/Selection_999093.png)

```C

shader_type canvas_item;
render_mode blend_mix;

uniform float PI = 3.1415926535897;

// h: [0 ,360]
// c: [0,1]
// g: [0,1]

float xyz_min( in float x, in float y, in float z ) {
	if ( x <= y && x <= z ) {
		return x;
	} else if ( y >= z ) {
		return y;
	}
	return z;
}

float xyz_max( in float x, in float y, in float z ) {
	if ( x >= y && x >= z ) {
		return x;
	} else if ( y >= z ) {
		return y;
	}
	return z;
}

vec4 hcg_convert(in float r1, in float g1, in float b1, in float m, in float a ) {
	return vec4((r1 + m), (g1 + m), (b1 + m), a);
}

vec4 hcg2rgb( in float h, in float c, in float g, in float a ) {
	float x = c * (1.0 - abs(mod((h / 60.0), 2.0) - 1.0));
	float m = g * (1.0 - c);
	if ( h < 60.0 ) { return hcg_convert(c, x, 0.0, m, a); }
	else if ( h < 120.0 ) { return hcg_convert(x, c, 0.0, m, a); }
	else if ( h < 180.0 ) { return hcg_convert(0.0, c, x, m, a); }
	else if ( h < 240.0 ) { return hcg_convert(0.0, x, c, m, a); }
	else if ( h < 300.0 ) { return hcg_convert(x, 0.0, c, m, a); }
	else { return hcg_convert(c, 0.0, x, m, a); }
}

vec4 rgb2hcg( in float r, in float g, in float b, in float a ) {
	float min = xyz_min(r,g,b);
	float max = xyz_max(r,g,b);
	float d = max - min;
	float h = 0.0;
	// UNTESTED!
	// original code was: float gr = min && min / (1.0 - d);
	float gr = min / (1.0 - d);
	if (r == max) { 
		float m = 0.0;
		if ( g < b ) { m = 1.0; }
		h = (g - b) / d + m * 6.0;
	} else if (g == max) {
		h = (b - r) / d + 2.0;
	} else { 
		h = (r - g) / d + 4.0;
	}
	h *= 60.0;
	return vec4( h,d,gr,a );
}

void fragment() {
	vec2 cUV = UV.xy - vec2(0.5,0.5);
	vec2 cUV2 = cUV * 2.0;
	float d = min( 2, cUV2.x * cUV2.x + cUV2.y * cUV2.y );
	float a = ( PI + atan( cUV2.y, cUV2.x ) ) / ( 2.0 * PI );
	COLOR = hcg2rgb( a * 360.0, d, 1.0, 1.0 );
}

```


## conversion of acterhd javascript implementation [DROPPED]

### js

original file: [github.com](https://github.com/acterhd/hcg-color/blob/master/convert/index.js)

```javascript

let mod = (a, n) => {
	return ((a % n) + n) % n;
};

const shift = [0.0, 2.0, 4.0];

let rgb2hcg = (rgb)=>{
	// see https://johnresig.com/blog/fast-javascript-maxmin/
	let m = Math.min.apply(Math, rgb);
	let M = Math.max.apply(Math, rgb);
	let C = M - m;
	let gbr = rgb.map((v, i, arr)=> { return arr[mod(i - 2, 3)]; });
	let brg = rgb.map((v, i, arr)=> { return arr[mod(i - 1, 3)]; });
	let G = C < 1 ? m / (1 - C) : 0;
	let H = C > 0 ? Math.max.apply(Math, rgb.map(function(v, i){
		let a = (gbr[i] - brg[i]) / C;
		let b = mod(a + shift[i], 6.0);
		return b * (M == v);
	})) : 0.0;
	return [H / 6.0, C, G];
};

let hcg2rgb = ([H, C, G])=>{
	let h = H * 6.0;
	let rgb = Array.from([h, h, h]).map(function(v, i){
		let a = mod(v - shift[i], 6.0);
		let b = Math.abs(a - 3.0) - 1.0;
		return Math.min(Math.max(b, 0.0), 1.0);
	});
	let m = G * (1 - C);
	return rgb.map(function(ch){
		return ch * C + m;
	});
};

```

### opengl es 3 shader

ref: http://docs.godotengine.org/en/3.0/tutorials/shading/shading_language.html

```C

float mod(a, n) {
	return ((a % n) + n) % n;
};

float vmin( in vec3 rgb ) {
	if ( rgb.r <= rgb.g && rgb.r <= rgb.b ) {
		return rgb.r;
	} else if ( rgb.g < rgb.b ) {
		return rgb.g;
	}
	return rgb.b;
}

float vmax( in vec3 rgb ) {
	if ( rgb.r >= rgb.g && rgb.r >= rgb.b ) {
		return rgb.r;
	} else if ( rgb.g > rgb.b ) {
		return rgb.g;
	}
	return rgb.b;
}

vec3 shift = vec3(0.0,2.0,4.0);

vec3 rgb2hcg( in vec3 rgb ) {
	float m = vmin(rgb);
	float M = vmax(rgb);
	float C = M - m;
	vec3 gbr = rgb.gbr;
	vec3 brg = rgb.brg;
	float G = C < 1.0 ? m / (1.0 - C) : 0.0;
	...
}

```

not finished, code is too nested and optimised for javascript to be easy to convert into C, lets try another implementation