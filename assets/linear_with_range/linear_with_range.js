var current = 0;
var target = 0;
var animated = 0;
var ranges = [[0,1], [0.1,0.9], [0.4,0.6], [0.6,0.4], [0.9,0.2]];
var range = [ 0, 1 ];
var STATE = 0;

function setup() {
  createCanvas( 800, 600 );
}

function keyPressed() {
  
  STATE += 1;
  STATE %= ranges.length;
  range = ranges[STATE]; 
  current = range_constrain( current );
  target = range_constrain( target );
  
}

function range_constrain( f ) {
  
  while( f < 0 ) { f += 1; }
  while( f > 1 ) { f -= 1; }
  
  if ( abs( range[0] - range[1] ) < 1 ) {
    
    if ( range[0] < range[1] ) {
      
      if ( f < range[0] ) { return range[0]; }
      if ( f > range[1] ) { return range[1]; }
      
    } else if ( range[0] > range[1] ) {
      
      if ( f < range[0] && f > range[1] ) {
        
        var d = f - range[1];
        if ( d > (range[0]-range[1]) * 0.5 ) {
          return range[0];
        } else {
          return range[1];
        }
        
      }
      
    }
    
  }
  print( "reached" );
  return f;
  
}

function dotted_line( from, size, w, x, y ) {
  
  noFill();
  stroke( 255,0,0 );
  strokeWeight( 3 );
  
  var dsize = 0.01;
  var totallength = 0;
  var break_request = false;
  
  // estimation of number of dot
  var dnum = ceil( size / dsize ) + 1;
  var start = from;
  var end = start + dsize;
  
  for ( var i = 0; i < dnum; ++i ) {
    if ( start >= 1 ) { start = 0; end = start + dsize; }
    if ( end > 1 ) { end = 1; }
    if ( totallength + dsize > size ) {
      var diff = totallength + dsize - size;
      end -= diff;
      break_request = true;
    }
    line( x + start * w, y, x + end * w, y );
    if ( break_request ) { break; }
    totallength += (end-start) + dsize;
    if ( totallength > size ) {
      break;
    }
    start = end + dsize;
    end += dsize * 2;
  }
  
  noStroke();
  
}

function draw() {
  
  background(0);
  
  var tx = 50.0;
  var ty = 100;
  var twidth = 700.0;
  var active_h = 40;
  
  if ( mouseY >= ty && mouseY <= ty + active_h ) { 
    target = range_constrain(  ( mouseX - tx ) / twidth );
  } else if ( mouseY < ty && mouseY >= ty - active_h ) {
    var prev = current;
    current = range_constrain(  ( mouseX - tx ) / twidth );
    if ( prev != current ) {
      animated = current;
    }
  }
  
  var left = 0;
  var right = 0;
  
  if ( current < target ) {
    left = ( 1+current ) - target;
    right = target - current;
  } else {
    left = current - target;
    right = ( 1+target ) - current;
  }
  
  // constraint
  if ( abs( range[0] - range[1] ) < 1 ) {
    if ( range[0] < range[1] ) {
      if ( current < target ) { left = 1; }
      if ( current > target ) { right = 1; }
    } else if ( range[0] > range[1] ) {
      if ( current < target && current <= range[1] && target >= range[1] ) { right = 1; }
      if ( current > target && current >= range[0] && target <= range[0] ) { left = 1; }
    }
  }
  
  if ( mouseY >= ty && mouseY <= ty + active_h ) {
    fill( 255, 50 );
    rect( tx, ty, twidth, active_h );
  } else {
    fill( 255, 20 );
    rect( tx, ty, twidth, active_h );
  }
  if ( mouseY < ty && mouseY >= ty - active_h ) {
    fill( 255, 50 );
    rect( tx, ty-active_h, twidth, active_h );
  } else {
    fill( 255, 20 );
    rect( tx, ty-active_h, twidth, active_h );
  }
  
  strokeWeight(1);
  stroke( 255,0,0 );
  line( tx, ty, tx + twidth, ty );
  
  strokeWeight(2);
  stroke( 255 );
  if ( range[0] < range[1] ) {
    line( tx+range[0]*twidth, ty, tx+range[1]*twidth, ty );
  } else {
    line( tx, ty, tx+range[1]*twidth, ty );
    line( tx+range[0]*twidth, ty, tx+twidth, ty );
  }
  
  noStroke();
  fill(255);
  text( "[0]="+range[0], (tx-4)+range[0]*twidth, ty-50 );
  text( "[1]="+range[1], (tx-4)+range[1]*twidth, ty-50 );
  strokeWeight(1);
  stroke( 255, 100 );
  line( tx+range[0]*twidth, ty, tx+range[0]*twidth, ty-45 );
  line( tx+range[1]*twidth, ty, tx+range[1]*twidth, ty-45 );
  
  strokeWeight(2);
  stroke( 0,255,255 );
  line( tx+current*twidth, ty-20, tx+current*twidth, ty );
  stroke( 255 );
  line( tx+target*twidth, ty, tx+target*twidth, ty+20 );
  
  noStroke();
  fill( 0,255,255 );
  if ( right < left ) {
    triangle( 
      tx+current*twidth, ty - 20,
      tx+current*twidth + 12, ty - 15,
      tx+current*twidth, ty - 10
      );
      dotted_line( current, right, twidth, tx, ty );
  } else if ( left < right ) {
    triangle( 
      tx+current*twidth, ty - 10,
      tx+current*twidth - 12, ty - 15,
      tx+current*twidth, ty - 20
      );
      dotted_line( target, left, twidth, tx, ty );
  }
  
  fill( 0,255,255 );
  text( int(current*100)/100, (tx-4)+current*twidth, ty-25 );
  fill( 255 );
  text( int(target*100)/100, (tx-4)+target*twidth, ty+35 );
  
  fill( 255 );
  text( "from current to target", 10, height - 45 );
  if ( left < right ) {
    text( "left: " + int(left*100)/100 + " [shortest]", 10, height - 30 );
    text( "right: " + int(right*100)/100, 10, height - 15 );
  } else {
    text( "left: " + int(left*100)/100, 10, height - 30 );
    text( "right: " + int(right*100)/100 + " [shortest]", 10, height - 15 );
  }
  
  text( "ranges", 10, height - 175 );
  for ( var i = 0; i < ranges.length; ++i ) {
    var tx = i + " : [" + ranges[i][0] + ", " + ranges[i][1] + "]";
    if ( i == STATE ) { tx += " SEL"; }
    text( tx, 10, height - 160 + i * 15 );
  }
  text( "(press any key to change)", 10, height - 160 + ranges.length * 15 );
  
  noFill();
  stroke( 255, 100 );
  strokeWeight(1);
  
  push();
  translate( width * 0.5, 365 );
  
  var radius = 200;
  
    circle( 0, 0, radius );
    stroke( 255 );
    if ( abs( range[1] - range[0] ) >= 1 ) {
      circle( 0,0, radius );
    } else {
      arc( 0,0, radius*2, radius*2, range[0] * PI * 2, range[1] * PI * 2 );
    }
    
    strokeWeight(2);
    stroke( 0, 255, 255 );
    var ccos = cos( current * PI * 2 );
    var csin = sin( current * PI * 2 );
    line( ccos * radius, csin * radius, ccos * (radius+20), csin * (radius+20) );
    
    stroke( 255 );
    ccos = cos( target * PI * 2 );
    csin = sin( target * PI * 2 );
    line( ccos * radius, csin * radius, ccos * (radius+20), csin * (radius+20) );
    
    strokeWeight(3);
    stroke( 255, 0, 0 );
    if ( right < left ) {
      arc( 0,0, radius*2, radius*2, current * PI * 2, target * PI * 2 );
    } else if ( left < right ) {
      arc( 0,0, radius*2, radius*2, target * PI * 2, current * PI * 2 );
    }
    
  pop();
  
  noStroke();
  
}